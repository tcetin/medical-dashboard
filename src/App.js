import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Dashboard from './components/Dashboard';
import DailyAnalitycs from './components/DailyAnalytics';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  appBarStyle:{
    backgroundColor:'#7a6e6e'
  }
});

class App extends Component {
  constructor(props) {
    super(props)
  }

  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };


  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <div className={classes.root}>
        <AppBar position="static" className= {classes.appBarStyle}>
          <Tabs value={value} onChange={this.handleChange}>
            <Tab label="Genel Hastane Bilgileri" />
            <Tab label="Yatış Bilgileri" />
            <Tab label="Mali Bilgiler" />
          </Tabs>
        </AppBar>
        {value === 0 && <DailyAnalitycs />}
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
