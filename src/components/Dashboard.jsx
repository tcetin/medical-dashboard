// import React from 'react';
// import PropTypes from 'prop-types';
// import classNames from 'classnames';
// import { withStyles } from '@material-ui/core/styles';
// import CssBaseline from '@material-ui/core/CssBaseline';
// import Typography from '@material-ui/core/Typography';
// import Paper from '@material-ui/core/Paper';
// import Grid from '@material-ui/core/Grid';
// import AmCharts from "@amcharts/amcharts3-react";
// import { ybYatakSayilari, ybYatakDoluluk, ybVentilatorSay, ybVentilatorDolulukOran, cihazBilgisi } from '../charts';

// AmCharts.theme = AmCharts.themes.dark;

// const drawerWidth = 240;

// const styles = theme => ({
//   root: {
//     display: 'flex',
//   },
//   toolbar: {
//     paddingRight: 24, // keep right padding when drawer closed
//   },
//   toolbarIcon: {
//     display: 'flex',
//     alignItems: 'center',
//     justifyContent: 'flex-end',
//     padding: '0 8px',
//     ...theme.mixins.toolbar,
//   },
//   appBar: {
//     zIndex: theme.zIndex.drawer + 1,
//     transition: theme.transitions.create(['width', 'margin'], {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//   },
//   appBarShift: {
//     marginLeft: drawerWidth,
//     width: `calc(100% - ${drawerWidth}px)`,
//     transition: theme.transitions.create(['width', 'margin'], {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//   },
//   menuButton: {
//     marginLeft: 12,
//     marginRight: 36,
//   },
//   menuButtonHidden: {
//     display: 'none',
//   },
//   title: {
//     flexGrow: 1,
//   },
//   drawerPaper: {
//     position: 'relative',
//     whiteSpace: 'nowrap',
//     width: drawerWidth,
//     transition: theme.transitions.create('width', {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.enteringScreen,
//     }),
//   },
//   drawerPaperClose: {
//     overflowX: 'hidden',
//     transition: theme.transitions.create('width', {
//       easing: theme.transitions.easing.sharp,
//       duration: theme.transitions.duration.leavingScreen,
//     }),
//     width: theme.spacing.unit * 7,
//     [theme.breakpoints.up('sm')]: {
//       width: theme.spacing.unit * 9,
//     },
//   },
//   appBarSpacer: theme.mixins.toolbar,
//   content: {
//     flexGrow: 1,
//     padding: theme.spacing.unit * 3,
//     height: '100vh',
//     overflow: 'auto',
//   },
//   chartContainer: {
//     marginLeft: -22,
//   },
//   tableContainer: {
//     height: 320,
//   },
//   h5: {
//     marginBottom: theme.spacing.unit * 2,
//   },
// });


// class Dashboard extends React.Component {
//   constructor(props) {
//     super(props);
//   }

//   componentDidMount() {

//   }



//   render() {
//     const { classes } = this.props;


//     return (
//       <div className={classes.root}>
//         <CssBaseline />
//         <main className={classes.content}>
//           <div className={classes.appBarSpacer} />
//           <Typography variant="h4" gutterBottom component="h2">
//             Yoğun Bakım Hizmet Bilgileri (2018 Ocak-Eylül)
//           </Typography>
//           <div className={classes.tableContainer}>
//             <Grid container spacing={24}>
//               <Grid item xs={6}>
//                 <Paper className={classes.paper}>
//                   <AmCharts.React style={{ width: "100%", height: "500px" }} options={ybYatakSayilari} />
//                 </Paper>
//               </Grid>
//               <Grid item xs={6}>
//                 <Paper className={classes.paper}>
//                   <AmCharts.React style={{ width: "100%", height: "500px" }} options={ybYatakDoluluk} />
//                 </Paper>
//               </Grid>
//               <Grid item xs={6}>
//                 <Paper className={classes.paper}>
//                   <AmCharts.React style={{ width: "100%", height: "500px" }} options={ybVentilatorSay} />
//                 </Paper>
//               </Grid>
//               <Grid item xs={6}>
//                 <Paper className={classes.paper}>
//                   <AmCharts.React style={{ width: "100%", height: "500px" }} options={ybVentilatorDolulukOran} />
//                 </Paper>
//               </Grid>
//               <div className={classes.appBarSpacer} />


//             </Grid>
            
//             <div className={classes.appBarSpacer} />
//             <Typography variant="h4" gutterBottom component="h2">
//               Cihaz Sayıları ve Çekim Sayıları (2018 Eylül)
//           </Typography>
//             <Grid container spacing={24}>
//               <Grid item xs={12}>
//                 <Paper className={classes.paper}>
//                   <AmCharts.React style={{ width: "100%", height: "500px" }} options={cihazBilgisi} />
//                 </Paper>
//               </Grid>
//             </Grid>
//           </div>
//         </main>
//       </div>
//     );
//   }
// }

// Dashboard.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

// export default withStyles(styles)(Dashboard);