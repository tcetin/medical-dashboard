import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import GunlukMuayene from '../charts/GunlukMuayene';
import YatakDoluluk from '../charts/YatakDoluluk';
import BasvuruTuru from '../charts/BasvuruTuru';
import AcilBasvuru from '../charts/AcilBasvuru';
import BransBazliBasvuru from '../charts/BransBazliBasvuru';
import BransBazliOrtBekleme from '../charts/BransBazliOrtBekleme';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing.unit * 9,
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 3,
        height: '100vh',
        overflow: 'auto',
    },
    chartContainer: {
        marginLeft: -22,
    },
    tableContainer: {
        height: 320,
    },
    h5: {
        marginBottom: theme.spacing.unit * 2,
    },
});


class DailyAnalytics extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {


    }



    render() {
        const { classes } = this.props;


        return (
            <div className={classes.root}>
                <CssBaseline />
                <main className={classes.content}>
                    <div className={classes.appBarSpacer} />
                    <div className={classes.tableContainer}>
                        <Grid container spacing={24}>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>
                                    <Typography variant="h6" gutterBottom component="h6">
                                        Günlük Muayene Sayısı
                                    </Typography>
                                    <GunlukMuayene/>
                                </Paper>
                            </Grid>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>
                                    <Typography variant="h6" gutterBottom component="h6">
                                        Yatak Doluluk Durumu
                                    </Typography>
                                    <YatakDoluluk/>
                                </Paper>
                            </Grid>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>
                                    <Typography variant="h6" gutterBottom component="h6">
                                        Başvuru Türüne Göre Hasta Sayıları
                                    </Typography>
                                    <BasvuruTuru/>
                                </Paper>
                            </Grid>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>
                                    <Typography variant="h6" gutterBottom component="h6">
                                        Acil Başvuru Bilgileri
                                    </Typography>
                                    <AcilBasvuru/>
                                </Paper>
                            </Grid>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>
                                    <Typography variant="h6" gutterBottom component="h6">
                                       Branş Bazlı Muayene Sayıları
                                    </Typography>
                                    <BransBazliBasvuru/>
                                </Paper>
                            </Grid>
                            <Grid item xs={4}>
                                <Paper className={classes.paper}>
                                    <Typography variant="h6" gutterBottom component="h6">
                                       Branş Bazlı Ortalama Bekleme Süreleri(dk)
                                    </Typography>
                                    <BransBazliOrtBekleme/>
                                </Paper>
                            </Grid>
                            <div className={classes.appBarSpacer} />
                        </Grid>
                    </div>
                </main>
            </div>
        );
    }
}

DailyAnalytics.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DailyAnalytics);