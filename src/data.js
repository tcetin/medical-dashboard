export default {
    yb_hizmet_bilgisi: {
        yatak_sayisi: [
            {
                tip: "1.derece",
                eriskin: 0,
                cocuk: 0,
                yenidogan: 7,
                eriskin_tescil: 0,
                cocuk_tescil: 0,
                yenidogan_tescil: 2
            },
            {
                tip: "2.derece",
                eriskin: 15,
                cocuk: 0,
                yenidogan: 0,
                eriskin_tescil: 16,
                cocuk_tescil: 0,
                yenidogan_tescil: 2
            },
            {
                tip: "3.derece",
                eriskin: 12,
                cocuk: 0,
                yenidogan: 0,
                eriskin_tescil: 12,
                cocuk_tescil: 0,
                yenidogan_tescil: 2
            }],
        yatak_doluluk_oran: [
            {
                tip: "1.derece",
                eriskin: 0,
                cocuk: 0,
                yenidogan: 37.8,
            },
            {
                tip: "2.derece",
                eriskin: 83.1,
                cocuk: 0,
                yenidogan: 0,
            },
            {
                tip: "3.derece",
                eriskin: 22.8,
                cocuk: 0,
                yenidogan: 0,
            }
        ],
        ventilator_sayisi: [
            {
                tip: "1.derece",
                eriskin: 0,
                cocuk: 0,
                yenidogan: 2,
            },
            {
                tip: "2.derece",
                eriskin: 7,
                cocuk: 0,
                yenidogan: 0,
            },
            {
                tip: "3.derece",
                eriskin: 13,
                cocuk: 0,
                yenidogan: 0,
            }
        ],
        ventilator_doluluk_oran: [
            {
                tip: "1.derece",
                eriskin: 0,
                cocuk: 0,
                yenidogan: 0,
            },
            {
                tip: "2.derece",
                eriskin: 175.6,
                cocuk: 0,
                yenidogan: 0,
            },
            {
                tip: "3.derece",
                eriskin: 23,
                cocuk: 0,
                yenidogan: 0,
            }
        ]
    },
    cihaz_bilgisi: {
        cihaz_sayilari: [
            {
                cihaz:"MR",
                sayi:1,
                cekim:999
            },
            {
                cihaz:"BT",
                sayi:1,
                cekim:440  
            },
            {
                cihaz:"USG",
                sayi:1,
                cekim:659    
            },
            {
                cihaz:"DOP.USG",
                sayi:1,
                cekim:138   
            },
            {
                cihaz:"KONV.RÖNT",
                sayi:1,
                cekim:0   
            },
            {
                cihaz:"DİJ.RÖNT",
                sayi:1,
                cekim:2486  
            },
            {
                cihaz:"SEYYAR.RÖNT",
                sayi:1,
                cekim:0  
            },
            {
                cihaz:"MAMOGRAFİ",
                sayi:1,
                cekim:21   
            },
            {
                cihaz:"EEG",
                sayi:1,
                cekim:1  
            },
            {
                cihaz:"EMG",
                sayi:1,
                cekim:2   
            },
            {
                cihaz:"ERCP",
                sayi:1,
                cekim:0   
            },
            {
                cihaz:"ESWT",
                sayi:1,
                cekim:0   
            },
            {
                cihaz:"FACO",
                sayi:1,
                cekim:7  
            },
            {
                cihaz:"KOLONOSKOPİ",
                sayi:1,
                cekim:19  
            },
            {
                cihaz:"NST",
                sayi:4,
                cekim:288
            },
            {
                cihaz:"EFORLU EKG",
                sayi:1,
                cekim:81
            },
            {
                cihaz:"GASTROSKOPİ",
                sayi:1,
                cekim:25
            },
            {
                cihaz:"LAPAROSKOPİ",
                sayi:1,
                cekim:16
            },
        ]
    }
}