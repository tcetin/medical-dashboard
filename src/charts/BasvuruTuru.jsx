import React, { Component } from 'react'

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_dark from "@amcharts/amcharts4/themes/dark";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";


am4core.useTheme(am4themes_dark);
am4core.useTheme(am4themes_animated);


export default class BasvuruTuru extends Component {
    componentDidMount() {
        // Create chart instance
        let chart = am4core.create("basvuru-turu", am4charts.PieChart);

        // Add data
        chart.data = [
            { "title": "İlk Başvuru", "value": 1500 },
            { "title": "Kansültasyon", "value": 500 },
            { "title": "Kontrol Muayenesi", "value": 500 },
        ];
        // Add label
        chart.innerRadius = 100;
        let label = chart.seriesContainer.createChild(am4core.Label);
        label.text = "2500";
        label.horizontalCenter = "middle";
        label.verticalCenter = "middle";
        label.fontSize = 50;
        // Add and configure Series
        let pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "value";
        pieSeries.dataFields.category = "title";

        // Add a legend
        chart.legend = new am4charts.Legend();
        chart.legend.valueLabels.template.text = "{value.value}";
    }
    render() {
        return (
            <div style={{ width: "100%", height: "500px" }} id="basvuru-turu" />
        )
    }
}
