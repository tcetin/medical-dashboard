
// import dt from '../data';

// export const ybYatakSayilari = {
//   "type": "serial",
//   "theme": "dark",
//   "legend": {
//     "horizontalGap": 10,
//     "maxColumns": 1,
//     "position": "right",
//     "useGraphSettings": true,
//     "markerSize": 10
//   },
//   "dataProvider": dt.yb_hizmet_bilgisi.yatak_sayisi,
//   "valueAxes": [{
//     "stackType": "regular",

//     /**
//      * A proprietary setting `stackByValue` which is not an
//      * official config option. It will be used by our custom
//      * plugin
//      */
//     "stackByValue": true,
//     "axisAlpha": 0.3,
//     "gridAlpha": 0
//   }],
//   "graphs": [{
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan_tescil"
//   }],
//   "categoryField": "tip",
//   "categoryAxis": {
//     "gridPosition": "start",
//     "axisAlpha": 0,
//     "gridAlpha": 0,
//     "position": "left"
//   },
//   "export": {
//     "enabled": true
//   },
//   "hideCredits": true,
//   "titles": [
//     {
//       "text": "Yatak Sayıları",
//       "size": 15,
//       "bold": false
//     }
//   ]
// };

// export const ybYatakDoluluk = {
//   "type": "serial",
//   "theme": "dark",
//   "legend": {
//     "horizontalGap": 10,
//     "maxColumns": 1,
//     "position": "right",
//     "useGraphSettings": true,
//     "markerSize": 10
//   },
//   "dataProvider": dt.yb_hizmet_bilgisi.yatak_doluluk_oran,
//   "valueAxes": [{
//     "stackType": "regular",

//     /**
//      * A proprietary setting `stackByValue` which is not an
//      * official config option. It will be used by our custom
//      * plugin
//      */
//     "stackByValue": true,
//     "axisAlpha": 0.3,
//     "gridAlpha": 0
//   }],
//   "graphs": [{
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan_tescil"
//   }],
//   "categoryField": "tip",
//   "categoryAxis": {
//     "gridPosition": "start",
//     "axisAlpha": 0,
//     "gridAlpha": 0,
//     "position": "left"
//   },
//   "export": {
//     "enabled": true
//   },
//   "hideCredits": true,
//   "titles": [
//     {
//       "text": "Yatak Doluluk Oranları(%)",
//       "size": 15,
//       "bold": false
//     }
//   ]
// };

// export const ybVentilatorSay = {
//   "type": "serial",
//   "theme": "dark",
//   "legend": {
//     "horizontalGap": 10,
//     "maxColumns": 1,
//     "position": "right",
//     "useGraphSettings": true,
//     "markerSize": 10
//   },
//   "dataProvider": dt.yb_hizmet_bilgisi.ventilator_sayisi,
//   "valueAxes": [{
//     "stackType": "regular",

//     /**
//      * A proprietary setting `stackByValue` which is not an
//      * official config option. It will be used by our custom
//      * plugin
//      */
//     "stackByValue": true,
//     "axisAlpha": 0.3,
//     "gridAlpha": 0
//   }],
//   "graphs": [{
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan_tescil"
//   }],
//   "categoryField": "tip",
//   "categoryAxis": {
//     "gridPosition": "start",
//     "axisAlpha": 0,
//     "gridAlpha": 0,
//     "position": "left"
//   },
//   "export": {
//     "enabled": true
//   },
//   "hideCredits": true,
//   "titles": [
//     {
//       "text": "Ventilatör Sayısı",
//       "size": 15,
//       "bold": false
//     }
//   ]
// };

// export const ybVentilatorDolulukOran = {
//   "type": "serial",
//   "theme": "dark",
//   "legend": {
//     "horizontalGap": 10,
//     "maxColumns": 1,
//     "position": "right",
//     "useGraphSettings": true,
//     "markerSize": 10
//   },
//   "dataProvider": dt.yb_hizmet_bilgisi.ventilator_doluluk_oran,
//   "valueAxes": [{
//     "stackType": "regular",

//     /**
//      * A proprietary setting `stackByValue` which is not an
//      * official config option. It will be used by our custom
//      * plugin
//      */
//     "stackByValue": true,
//     "axisAlpha": 0.3,
//     "gridAlpha": 0
//   }],
//   "graphs": [{
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Erişkin - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "eriskin_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Çocuk - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "cocuk_tescil"
//   },
//   {
//     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
//     "fillAlphas": 0.8,
//     "labelText": "[[value]]",
//     "labelPosition": "middle",
//     "lineAlpha": 0.3,
//     "title": "Yenidoğan - Tescil",
//     "type": "column",
//     "color": "#000000",
//     "valueField": "yenidogan_tescil"
//   }],
//   "categoryField": "tip",
//   "categoryAxis": {
//     "gridPosition": "start",
//     "axisAlpha": 0,
//     "gridAlpha": 0,
//     "position": "left"
//   },
//   "export": {
//     "enabled": true
//   },
//   "hideCredits": true,
//   "titles": [
//     {
//       "text": "Ventilatör Doluluk Oranları(%)",
//       "size": 15,
//       "bold": false
//     }
//   ]
// };

// export const cihazBilgisi = {
//   "type": "serial",
//   "theme": "dark",
//   "legend": {
//     "equalWidths": false,
//     "useGraphSettings": true,
//     "valueAlign": "left",
//     "valueWidth": 120
//   },
//   "dataProvider": dt.cihaz_bilgisi.cihaz_sayilari,
//   "valueAxes": [{
//     "id": "sayiAxis",
//     "axisAlpha": 0,
//     "gridAlpha": 0,
//     "position": "left",
//     "title": "Cihaz Sayısı"
//   }, {
//     "id": "cekimAxis",
//     "axisAlpha": 0,
//     "gridAlpha": 0,
//     "labelsEnabled": false,
//     "position": "right"
//   }],
//   "graphs": [{
//     "alphaField": "alpha",
//     "balloonText": "[[cihaz]]:[[sayi]]",
//     "dashLengthField": "dashLength",
//     "fillAlphas": 0.7,
//     "legendPeriodValueText": "Toplam: [[value.sum]]",
//     "legendValueText": "[[cihaz]]",
//     "title": "Cihaz Sayısı",
//     "type": "column",
//     "valueField": "sayi",
//     "valueAxis": "sayiAxis"
//   },
//   {
//     "balloonText": "Çekim Sayısı:[[value]]",
//     "bullet": "round",
//     "bulletBorderAlpha": 1,
//     "useLineColorForBulletBorder": true,
//     "bulletColor": "#FFFFFF",
//     "bulletSizeField": "sayi",
//     "dashLengthField": "dashLength",
//     "descriptionField": "cihaz",
//     "labelPosition": "right",
//     "labelText": "",
//     "legendValueText": "[[value]]",
//     "title": "Çekim Sayısı",
//     "fillAlphas": 0,
//     "valueField": "cekim",
//     "valueAxis": "cekimAxis"
//   }],
//   "chartCursor": {
//     "categoryBalloonDateFormat": "DD",
//     "cursorAlpha": 0.1,
//     "cursorColor": "#000000",
//     "fullWidth": true,
//     "valueBalloonsEnabled": false,
//     "zoomable": false
//   },
//   "categoryField": "cihaz",
//   "export": {
//     "enabled": true
//   },
//   "hideCredits": true
// }

export * from './GunlukMuayene'