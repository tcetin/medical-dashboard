import React, { Component } from 'react'

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_dark from "@amcharts/amcharts4/themes/dark";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";


am4core.useTheme(am4themes_dark);
am4core.useTheme(am4themes_animated);


export default class BransBazliOrtBekleme extends Component {
    componentDidMount() {
        let chart = am4core.create("brans-bazli-ort-bekleme", am4charts.XYChart);

        chart.data = [{
            "division": "Genel Cerrahi",
            "wait-time": 50,
            "div-avg-time":45
        },
        {
            "division": "Nöroloji",
            "wait-time": 45,
            "div-avg-time":45
        },
        {
            "division": "Kadın Doğum",
            "wait-time": 45,
            "div-avg-time":55
        },
        {
            "division": "Göz Hastalıkları",
            "wait-time": 45,
            "div-avg-time":60
        },
        {
            "division": "Ortopedi",
            "wait-time": 45,
            "div-avg-time":50
        },
        {
            "division": "Çocuk Hastalıkları",
            "wait-time": 45,
            "div-avg-time":40
        },
        {
            "division": "Kardiyoloji",
            "wait-time": 45,
            "div-avg-time":65
        }];
        
        //create category axis for years
        let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "division";
        categoryAxis.renderer.inversed = true;
        categoryAxis.renderer.grid.template.location = 0;
        
        //create value axis for income and expenses
        let valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.opposite = true;
        
        
        //create columns
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.categoryY = "division";
        series.dataFields.valueX = "div-avg-time";
        series.name = "Ortalama Bekleme Süresi(Branş)";
        series.columns.template.fillOpacity = 0.5;
        series.columns.template.strokeOpacity = 0;
        series.tooltipText = "{categoryY}: {valueX.value}";
        
        //create line
        let lineSeries = chart.series.push(new am4charts.LineSeries());
        lineSeries.dataFields.categoryY = "division";
        lineSeries.dataFields.valueX = "wait-time";
        lineSeries.name = "Ort. Bekleme Süresi(Genel)";
        lineSeries.strokeWidth = 3;
        lineSeries.tooltipText = "Ort. Bekleme Süresi(Genel): {valueX.value}";
        lineSeries.strokeDasharray = 3;

        
        //add bullets
        let circleBullet = lineSeries.bullets.push(new am4charts.CircleBullet());
        circleBullet.circle.fill = am4core.color("#fff");
        circleBullet.circle.strokeWidth = 2;
        
        //add chart cursor
        chart.cursor = new am4charts.XYCursor();
        chart.cursor.behavior = "zoomY";
        
        //add legend
        chart.legend = new am4charts.Legend();

    }
    render() {
        return (
            <div style={{ width: "100%", height: "500px" }} id="brans-bazli-ort-bekleme" />
        )
    }
}
